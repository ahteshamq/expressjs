const express = require('express')
const app = express()
const port = process.env.PORT || 3000;

app.get('/groceries', (req, res) => {
    res.send([
        {
            item: "Milk",
            quantity: 2
        },
        {
            item: "Bread",
            quantity: 1
        },
        {
            item: "Chocolate",
            quantity: 2
        }
    ])
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})